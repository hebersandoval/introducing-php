<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Using server-side includes</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<h1>Including External Files</h1>
<p>This paragraph is in the original file.</p>
<!-- "include" is a language construct not a method -->
<?php include_once './includes/para.html' ?>
<p>This is also in the original file.</p>
<?php include_once './includes/para.html' ?>
<?php require './includes/copyright.php' ?>

<!-- before calling a function on a external directory, the file source must be declared first. -->
<p><?= lyn_copyright(2017); ?> Heber Sandoval</p>
</body>
</html>
