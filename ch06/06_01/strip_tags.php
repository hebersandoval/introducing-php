<?php

$input = "<p>Hi, there! <script>alert('Gotcha!');</script>. <a href='http://www.lynda.com/PHP-training-tutorials/282-0.html'>Expand your PHP skills</a>.";

// will strip the <script> tag and returns it as plain text. you can pass another argument to this function that will not strip the specified tags, in this case it will show the <p> and <a> tags
echo strip_tags($input, '<p><a>');
