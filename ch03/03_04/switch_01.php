<?php
$name = 'Mike';

// switch ($name) {
//     case 'Arthur Dent':
//         echo 'I could never get the hang of Thursdays.';
//         break;
//     case 'Marvin':
//     case 'Paranoid Android':
//     case 'robot':
//         echo "I've got this terrible pain in all the diodes down my left-hand side.";
//         break;
//     default:
//         echo 'Is that really a piece of fairy cake?';
// }
switch ($name) {
  case 'Joe':
    echo "$name likes burgers";
    break;
  case 'Mike':
    echo "$name likes coconuts";
    break;
  default:
    echo 'Who are you?';
    break;
}
