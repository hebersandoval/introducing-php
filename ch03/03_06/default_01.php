<?php
$unit_cost = 0; // php considers 0 as false.

// the `??` is called the "null-coalesce" operator. this operator allow you to use "0", or any value that its 'falsey', as a real value for this scenario.

// $wholesale_price = $unit_cost ?? 25;

// used for php 5.
// `isset($unit_cost)` is setting the value to 0.
// then `$unit_cost` is checking for a T or F value.

if (isset($unit_cost) && $unit_cost) {
  $wholesale_price = $unit_cost;
} else {
  $wholesale_price = 25;
}

echo $wholesale_price;
