<?php
$name = 'Arthur Dent';
$day = 'Monday';

if ($name == 'Arthur Dent' && $day == 'Sunday') {
    echo 'I could never get the hang of Thursdays.';
} elseif ($name == 'Marvin' && $day == 'Monday') {
    echo "I've got this terrible pain in all the diodes down my left-hand side.";
} else {
    echo 'Is that really a piece of fairy cake?';
}
