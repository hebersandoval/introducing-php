<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Challenge: using loops</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<h1>Multiplication Table</h1>
<table>
  <?php
    // create first 1st row of table headers
    echo '<tr>';
    echo '<th>&nbsp;</th>';
    for ($col = 1; $col <= 12 ; $col++) {
      // wil create <th> tags with the value of $col until it has met the condition
      echo "<th>$col</th>";
    }

    // create remaining rows
    for ($row = 1, $col = 1; $row <= 12; $row++) :
      echo '<tr>';
      // first cell is a table header
      if ($col == 1) {
        // will increment the initializer and execute until the condition is met, while returning the value of $row inside <th> tags
        echo "<th>$row</th>";
      }
      while ($col <= 12) {
        echo '<td>' . $row * $col++ . '</td>';
      }
      echo '</tr>';
      $col = 1;
    endfor;
  ?>
</table>
</body>
</html>
