<?php
$descriptions = [
    'Earth' => 'mostly harmless',
    'Marvin' => 'the paranoid android',
    'Zaphod Beeblebrox' => 'President of the Imperial Galactic Government'
];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Using a foreach loop</title>
    <link rel="stylesheet" href="styles.css" type="text/css">
</head>
<body>
<h1>Descriptions</h1>
  <?php
  // foreach iterates over each item in an array or associative array(key, value pair), storing the values in a temporary variable, which are only scoped inside its block.

    foreach ($descriptions as $key => $value) {
      echo "<p>$key is $value.</p>";
    }
  ?>
</body>
</html>
