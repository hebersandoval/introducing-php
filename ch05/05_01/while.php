<?php
// initialize counter
$i = 0;

while ($i < 10) {
    // increment counter
    $i++;
    if ($i % 2) { // will skip over any odd numbers
      continue;
    }
    echo $i . '<br>';
    // breaking out the the loop early
    // if ($i == 6) {
    //   break;
    // }
}
