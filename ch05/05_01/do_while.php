<?php
// initialize counter
$i = 100;

do {
    // increment counter
    // will execute at least once
    $i++;
    echo $i . '<br>';
} while ($i < 10);
