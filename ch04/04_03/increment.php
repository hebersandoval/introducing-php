<?php
$number = 5;

// increment operator after variable
$number++;
echo '$number++: ' . $number . '<br>';

// increment operator before variable, which changes the value FIRST and the new value becomes part of the calculation
++$number;
echo '++$number: ' . $number . '<br>';

// when increment operator in after the variable, it does the calculation FIRST and then increments the current value. since $number is currently 7, it multiples that value by 2, which is 14
$result = $number++ * 2;

echo '$result is ' . $result . '<br>';
echo $number . '<br>';
